import {
    NOT_LOGIN_YET,
    LOGIN_USER_SUCCESS
} from './types';

export const alreadyLogin = (user) => {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: user
    }
}

export const notLoginYet = () => {
    return {
        type: NOT_LOGIN_YET
    };
};