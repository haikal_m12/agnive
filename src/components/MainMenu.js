import React from 'react';
import  { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import Home from './Home';

export default createAppContainer(createBottomTabNavigator(
    {
        Home: Home,
        // PostPhoto: PostPhoto,
        // Profile: {
        //     screen: ({ screenProps }) => <ProfileDrawer screenProps={{ rootStackNavigator: screenProps.rootStackNavigator }} />
        // }
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Home') {
                    iconName = `home`;
                } 
                // else if (routeName === 'PostPhoto') {
                //     iconName = `add-box`;
                // } else if (routeName === 'Profile') {
                //     iconName = `account-box`
                // }

                // You can return any component that you like here!
                return <Icon name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: '#4488d6',
            inactiveTintColor: 'gray',
            showLabel: false
        },
    }
));