import React, { Component } from 'react';
import { View } from 'react-native'
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {alreadyLogin, notLoginYet} from '../redux/actions';
import Main from './Main';

class AppInit extends Component {
    componentDidMount() {
        this.keepLogin();
    }

    keepLogin = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('@storage_Key')
            // return jsonValue != null ? JSON.parse(jsonValue) : null
            if(jsonValue !== null) {
                this.props.alreadyLogin(JSON.parse(jsonValue));
            } else {
                this.props.notLoginYet();
            }
          } catch(e) {
            console.log(e)
          }
        
          console.log('Done.')
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Main />
            </View>
        );
    }
}


export default connect(null , {
    alreadyLogin,
    notLoginYet
})(AppInit);