import axios from 'axios';
import {
    // EMAIL_LOGIN_CHANGED,
    // PASSWORD_LOGIN_CHANGED,
    LOGIN_USER,
    LOGIN_USER_FAIL,
    LOGIN_USER_SUCCESS
} from './types';
import Api_Url from '../../supports/api_url';
import AsyncStorage from '@react-native-community/async-storage';

const storeData = async (value) => {
    console.log(value)
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('@storage_Key', jsonValue)
    } catch (e) {
        console.log(error);
    }
}

export const loginUser = (email, password) => {
    return (dispatch) => {
        dispatch({
            type: LOGIN_USER
        })

        if (email !== '' && password !== '') {
            let obj = {
                email, 
                password
            }

            console.log(obj)
            // let options = {
            //     headers: {
            //         "Access-Control-Allow-Origin": "*"
            //     }
            // }
            
            axios.post(`${Api_Url}/user/login`, obj)
            .then((res) => {
                console.log(res.data.user)
                // storeData(res.data.user);
                // dispatch({
                //     type: LOGIN_USER_SUCCESS,
                //     payload: res.data.user
                // })
            })
            .catch((err) => {
                console.log(err)
                dispatch({
                    type: LOGIN_USER_FAIL,
                    payload: err.message
                })
            })
        } else {
            console.log(err)
            dispatch({
                type: LOGIN_USER_FAIL,
                payload: "Tidak boleh kosong"
            })
        }
    }
}