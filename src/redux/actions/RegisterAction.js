import axios from 'axios';
import {
    // EMAIL_REGISTER_CHANGED,
    // USERNAME_REGISTER_CHANGED,
    // PASSWORD_REGISTER_CHANGED,
    // CON_PASSWORD_REGISTER_CHANGED,
    REGISTER_USER,
    REGISTER_USER_FAIL,
    REGISTER_USER_SUCCESS,
    LOGIN_USER_SUCCESS
} from './types';


export const registerUser = (email, username, password, conPassword) => {
    return(dispatch) => {
        dispatch({
            type: REGISTER_USER
        })

        if(email !== '' && username !== '' && password !== '' && conPassword !== '' ){
            if(password === conPassword) {
                
            } else {
                dispatch({ type: REGISTER_USER_FAIL, payload: 'Password and Confirm Password must be same!' })
            }
        
        } else {
            dispatch({type: REGISTER_USER_FAIL, payload: 'All Form Must be Filled!'})
        }
    }
}