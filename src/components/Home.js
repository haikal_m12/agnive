import React, { Component } from 'react';
import { View, Text, Platform, Image, ScrollView } from 'react-native';
import { Header } from 'react-native-elements'
import { Container, Content, Card, CardItem, Thumbnail, Icon, Left, Body } from 'native-base';
import { connect } from 'react-redux';

class Home extends Component {
    state: {

    }

    componentDidUpdate() {
        if (!this.props.user) {
            this.props.screenProps.rootStackNavigator.navigate('Login');
        }
    }


    render() {
        return (
            <Container style={{flex: 1}}>
                <View>
                    <Header
                        leftComponent={{
                            text: 'Aplikasi',
                            style: {
                                color: 'black',
                                fontSize: 18
                            }
                        }}
                        leftContainerStyle={{ flex: 3 }}
                        containerStyle={{
                            backgroundColor: '#fff',
                            justifyContent: 'space-around',
                            marginTop: Platform.OS === 'ios' ? 0 : -30
                        }}
                    />
                </View>
                <ScrollView>
                    <Content>
                        <View style={{marginHorizontal: 10}}>
                            <Card>
                                <CardItem>
                                    <Body>
                                        <Text>{this.props.user}</Text>
                                        <Text note>Role: {this.props.role}</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </View>
                    </Content>
                </ScrollView>
            </Container>
        )
    }
}

const mapStateToProps = ({auth}) => {
    return {
        user: auth.user ? auth.user : null,
        email: auth.email ?auth.email: null,
        phone: auth.phone ?  auth.phone : null,
        role: auth.role ?  auth.role : null
    }
}

export default connect(mapStateToProps)(Home);